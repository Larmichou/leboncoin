//
//  ListViewController.swift
//  LeBonCoin
//
//  Created by Armel Fardel on 09/02/2020.
//  Copyright © 2020 Armel Fardel. All rights reserved.
//

import UIKit

protocol ItemListPresenter {
    func showItems()
    func showError(_ error: Error)
    func showFilters(categories: [Category])
    func filter(onCategory category: Category?)
}

class ListViewController: UIViewController {

    var items: [Item] = [] {
        didSet {
            items.sort(by: {
                if $0.urgent == $1.urgent {
                    return $0.creationDate > $1.creationDate
                } else {
                    return $0.urgent
                }
            })
        }
    }
    
    var filteredItems: [Item] = []
    
    lazy var tableView: UITableView = {
        let table = UITableView(frame: self.view.bounds)
        table.delegate = self
        table.dataSource = self
        table.register(ItemTableViewCell.self, forCellReuseIdentifier: ItemTableViewCell.cellIdentifier)
        self.view.addSubview(table)
        table.translatesAutoresizingMaskIntoConstraints = false
        table.leadingAnchor.constraint(equalTo: self.view.leadingAnchor).isActive = true
        table.trailingAnchor.constraint(equalTo: self.view.trailingAnchor).isActive = true
        table.topAnchor.constraint(equalTo: self.view.topAnchor).isActive = true
        table.bottomAnchor.constraint(equalTo: self.view.bottomAnchor).isActive = true
        return table
    }()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.title = "List"
        view.backgroundColor = .white
        showItems()
    }
    
    func prepareFilterButton() {
        if RequesterManager.shared.categories.count > 0 {
            let button = UIBarButtonItem(title: "Filter", style: .done, target: self, action: #selector(didPushFilter(_:)))
            self.navigationItem.rightBarButtonItem = button
        } else {
            self.navigationItem.rightBarButtonItem = nil
        }
    }
    
    @objc func didPushFilter(_ sender: UIBarButtonItem?) {
        showFilters(categories: RequesterManager.shared.categories)
    }
}

extension ListViewController: ItemListPresenter {
    
    func showItems() {
        RequesterManager.shared.retrieveItems { [weak self] result in
            DispatchQueue.main.async {
                switch result {
                case .success(let items):
                    self?.items = items
                    self?.filter(onCategory: nil)
                case .failure(let error):
                    self?.showError(error)
                }
                self?.prepareFilterButton()
            }
        }
    }
    
    func showError(_ error: Error) {
        let alert = UIAlertController(title: "Error",
                                      message: error.localizedDescription,
                                      preferredStyle: .alert)
        let okAction = UIAlertAction(title: "Ok", style: .default, handler: nil)
        alert.addAction(okAction)
        let retryAction = UIAlertAction(title: "Retry", style: .default) { [weak self] _ in
            self?.showItems()
        }
        alert.addAction(retryAction)
        present(alert, animated: true, completion: nil)
    }
    
    func showFilters(categories: [Category]) {
        let actionSheet = UIAlertController(title: "Filter on",
                                            message: "Select category",
                                            preferredStyle: .actionSheet)
        let noFilterAction = UIAlertAction(title: "All categories", style: .default) { [weak self] _ in
            self?.filter(onCategory: nil)
        }
        actionSheet.addAction(noFilterAction)
        
        categories.forEach({ aCategory in
            let categoryAction = UIAlertAction(title: aCategory.name, style: .default) { [weak self] _ in
                self?.filter(onCategory: aCategory)
            }
            actionSheet.addAction(categoryAction)
        })
        
        let cancelAction = UIAlertAction(title: "Cancel", style: .cancel, handler: nil)
        actionSheet.addAction(cancelAction)
        
        if UIDevice.current.userInterfaceIdiom == .pad {
            if let popoverController = actionSheet.popoverPresentationController {
                popoverController.barButtonItem = navigationItem.rightBarButtonItem
            }
        }
        
        present(actionSheet, animated: true, completion: nil)
        
    }
    
    func filter(onCategory category: Category?) {
        if let category = category {
            self.filteredItems = items.filter({ $0.categoryId == category.id })
        } else {
            self.filteredItems = items
        }
        tableView.reloadData()
    }
}

extension ListViewController: UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return filteredItems.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: ItemTableViewCell.cellIdentifier, for: indexPath) as? ItemTableViewCell else { return UITableViewCell() }
        cell.update(filteredItems[indexPath.row])
        return cell
    }
}

extension ListViewController: UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 60.0
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        showNextViewController()
    }
}

extension ListViewController: Routing {
    func showNavigationController() {
        navigationController?.setNavigationBarHidden(false, animated: false)
        showBackButton()
    }
    
    func showNextViewController() {
        guard let selectedRow = tableView.indexPathForSelectedRow?.row else { return }
        let item = filteredItems[selectedRow]
        let nextVC = ItemDetailsViewController()
        nextVC.showItem(item)
        navigationController?.pushViewController(nextVC, animated: true)
    }
    
    func showBackButton() {
        navigationItem.hidesBackButton = true
    }
}
