//
//  LoadingViewController.swift
//  LeBonCoin
//
//  Created by Armel Fardel on 11/02/2020.
//  Copyright © 2020 Armel Fardel. All rights reserved.
//

import UIKit

protocol Loader {
    func showLoading(_ loading: Bool)
    func showError(_ error: Error)
    func launchRequest()
}

class LoadingViewController: UIViewController, Loader {

    lazy var spinner: UIActivityIndicatorView = {
        let indicator: UIActivityIndicatorView
        if #available(iOS 13.0, *) {
            indicator = UIActivityIndicatorView(style: .large)
        } else {
            indicator = UIActivityIndicatorView(style: .whiteLarge)
        }
        indicator.translatesAutoresizingMaskIntoConstraints = false
        self.view.addSubview(indicator)
        indicator.centerXAnchor.constraint(equalTo: self.view.centerXAnchor).isActive = true
        indicator.centerYAnchor.constraint(equalTo: self.view.centerYAnchor).isActive = true
        return indicator
    }()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        view.backgroundColor = .white
    }
    
    func launchRequest() {
        showLoading(true)

        RequesterManager.shared.retrieveCategories { [weak self] (result) in
            DispatchQueue.main.async {
                switch result {
                case .success:
                    self?.showLoading(false)
                    self?.showNextViewController()
                case .failure(let error):
                    self?.showError(error)
                }
            }
        }
    }
    
    func showLoading(_ loading: Bool) {
        if loading {
            spinner.startAnimating()
        } else {
            spinner.stopAnimating()
        }
    }
    
    func showError(_ error: Error) {
        showLoading(false)
        let alert = UIAlertController(title: "Error",
                                      message: error.localizedDescription,
                                      preferredStyle: .alert)
        let okAction = UIAlertAction(title: "Ok", style: .default, handler: nil)
        alert.addAction(okAction)
        let retryAction = UIAlertAction(title: "Retry", style: .default) { [weak self] _ in
            self?.launchRequest()
        }
        alert.addAction(retryAction)
        present(alert, animated: true, completion: nil)
    }
}

extension LoadingViewController: Routing {
    func showNavigationController() {
        navigationController?.setNavigationBarHidden(true, animated: false)
    }
    
    func showNextViewController() {
        let nextVC = ListViewController()
        navigationController?.pushViewController(nextVC, animated: true)
    }
    
    func showBackButton() {
        navigationItem.hidesBackButton = true
    }
}
