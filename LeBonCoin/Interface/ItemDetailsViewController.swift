//
//  ItemDetailsViewController.swift
//  LeBonCoin
//
//  Created by Armel Fardel on 14/02/2020.
//  Copyright © 2020 Armel Fardel. All rights reserved.
//

import UIKit

protocol ItemDetailsPresenter {
    func showItem(_ item: ItemDetailsPresentable)
}

protocol ItemDetailsPresentable {
    var detailsCategory: String { get }
    var detailsTitle: String { get }
    var detailsPrice: String { get }
    var detailsDescriptions: String { get }
    var detailsImageURL: URL? { get }
    var detailsDateString: String? { get }
}

extension Item: ItemDetailsPresentable {
    
    static var dateFormatter: DateFormatter {
        let formatter = DateFormatter()
        formatter.locale = .current
        formatter.timeStyle = .short
        formatter.dateStyle = .medium
        return formatter
    }
    
    var detailsCategory: String {
        return RequesterManager.shared.categories.first(where: { $0.id == categoryId })?.name ?? ""
    }
    
    var detailsTitle: String {
        return title
    }
    
    var detailsDescriptions: String {
        return desc
    }
    
    var detailsPrice: String {
        return Item.currencyFormatter.string(from: NSNumber(value: price)) ?? "Unknown"
    }
    
    var detailsImageURL: URL? {
        return imagesURLs.thumbnail
    }
    
    var detailsDateString: String? {
        return Item.dateFormatter.string(from: creationDate)
    }
}

class ItemDetailsViewController: UIViewController, ItemDetailsPresenter {

    
    lazy var titleLabel: UILabel = {
        let label = UILabel()
        label.font = .boldSystemFont(ofSize: 16)
        label.numberOfLines = 0
        label.translatesAutoresizingMaskIntoConstraints = false
        self.view.addSubview(label)
        label.topAnchor.constraint(equalTo: self.iconView.bottomAnchor, constant: 8).isActive = true
        label.leadingAnchor.constraint(equalTo: self.iconView.leadingAnchor).isActive = true
        label.trailingAnchor.constraint(equalTo: self.iconView.trailingAnchor).isActive = true
        label.heightAnchor.constraint(greaterThanOrEqualToConstant: 20).isActive = true
        return label
    }()
    
    lazy var iconView: UIImageView = {
        let imageView = UIImageView()
        imageView.contentMode = .scaleAspectFit
        imageView.clipsToBounds = true
        imageView.backgroundColor = .clear
        imageView.translatesAutoresizingMaskIntoConstraints = false
        self.view.addSubview(imageView)
        imageView.leadingAnchor.constraint(equalTo: self.view.leadingAnchor, constant: 16).isActive = true
        imageView.trailingAnchor.constraint(equalTo: self.view.trailingAnchor, constant: -16).isActive = true
        imageView.topAnchor.constraint(equalTo: self.view.topAnchor, constant: self.view.layoutMargins.top + 50).isActive = true
        imageView.heightAnchor.constraint(lessThanOrEqualToConstant: 200).isActive = true
        return imageView
    }()
    
    lazy var categoryLabel: UILabel = {
        let label = UILabel()
        label.font = .italicSystemFont(ofSize: 14)
        label.textColor = .lightGray
        label.numberOfLines = 0
        label.translatesAutoresizingMaskIntoConstraints = false
        self.view.addSubview(label)
        label.topAnchor.constraint(equalTo: self.titleLabel.bottomAnchor, constant: 8).isActive = true
        label.leadingAnchor.constraint(equalTo: self.titleLabel.leadingAnchor).isActive = true
        label.trailingAnchor.constraint(equalTo: self.titleLabel.trailingAnchor).isActive = true
        label.heightAnchor.constraint(greaterThanOrEqualToConstant: 16).isActive = true
        return label
    }()
    
    
    lazy var detailsLabel: UITextView = {
        let textView = UITextView()
        textView.isEditable = false
        textView.isSelectable = false
        textView.font = .systemFont(ofSize: 14)
        textView.textColor = .darkGray
        textView.translatesAutoresizingMaskIntoConstraints = false
        self.view.addSubview(textView)
        textView.topAnchor.constraint(equalTo: self.categoryLabel.bottomAnchor, constant: 8).isActive = true
        textView.leadingAnchor.constraint(equalTo: self.titleLabel.leadingAnchor).isActive = true
        textView.trailingAnchor.constraint(equalTo: self.titleLabel.trailingAnchor).isActive = true
        textView.heightAnchor.constraint(greaterThanOrEqualToConstant: 30).isActive = true
        return textView
    }()
    
    lazy var priceLabel: UILabel = {
        let label = UILabel()
        label.font = .boldSystemFont(ofSize: 18)
        label.numberOfLines = 1
        label.textAlignment = .right
        label.translatesAutoresizingMaskIntoConstraints = false
        self.view.addSubview(label)
        
        label.topAnchor.constraint(equalTo: self.detailsLabel.bottomAnchor, constant: 8).isActive = true
        label.leadingAnchor.constraint(equalTo: self.detailsLabel.leadingAnchor).isActive = true
        label.trailingAnchor.constraint(equalTo: self.detailsLabel.trailingAnchor).isActive = true
        label.heightAnchor.constraint(greaterThanOrEqualToConstant: 16).isActive = true
        label.bottomAnchor.constraint(equalTo: self.view.bottomAnchor, constant: -8).isActive = true
        return label
    }()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.view.backgroundColor = .white
        self.title = "Details"
        
    }
    
    func showItem(_ item: ItemDetailsPresentable) {
        titleLabel.text = item.detailsTitle
        detailsLabel.text = item.detailsDescriptions
        if let dateString = item.detailsDateString {
            categoryLabel.text = item.detailsCategory + " (" + dateString + ")"
        } else {
            categoryLabel.text = item.detailsCategory
        }
        priceLabel.text = item.detailsPrice
        self.iconView.retrieveImage(fromURL: item.detailsImageURL, completion: { [weak self] (image, url) in
            DispatchQueue.main.async {
                self?.iconView.backgroundColor = image == nil ? .lightGray : .clear
                self?.iconView.image = image
            }
        })
    }
}

extension ItemDetailsViewController: Routing {
    func showNavigationController() {
        navigationController?.setNavigationBarHidden(false, animated: false)
    }
    
    func showNextViewController() {
    }
    
    func showBackButton() {
        navigationItem.hidesBackButton = false
    }
}
