//
//  ItemTableViewCell.swift
//  LeBonCoin
//
//  Created by Armel Fardel on 11/02/2020.
//  Copyright © 2020 Armel Fardel. All rights reserved.
//

import UIKit

protocol ItemCellPresentable {
    var cellCategory: String { get }
    var cellTitle: String { get }
    var cellUrgence: Bool { get }
    var cellPrice: String { get }
    var cellImageURL: URL? { get }
}

extension Item: ItemCellPresentable {
    
    static var currencyFormatter: NumberFormatter {
        let formatter = NumberFormatter()
        formatter.numberStyle = .currency
        formatter.locale = Locale(identifier: "fr_FR")
        return formatter
    }

    var cellCategory: String {
        return RequesterManager.shared.categories.first(where: { $0.id == categoryId })?.name ?? ""
    }
    
    var cellTitle: String {
        return title
    }
    
    var cellUrgence: Bool {
        return urgent
    }
    
    var cellPrice: String {
        return Item.currencyFormatter.string(from: NSNumber(value: price)) ?? "Unknown"
    }
    
    var cellImageURL: URL? {
        return imagesURLs.small
    }
}

class ItemTableViewCell: UITableViewCell {
    static let cellIdentifier = "ItemTableViewCellIdentifier"

    lazy var titleLabel: UILabel = {
        let label = UILabel()
        label.font = .boldSystemFont(ofSize: 13)
        label.numberOfLines = 1
        label.translatesAutoresizingMaskIntoConstraints = false
        self.contentView.addSubview(label)
        label.topAnchor.constraint(equalTo: self.contentView.topAnchor, constant: 8).isActive = true
        label.leadingAnchor.constraint(equalTo: self.iconView.trailingAnchor, constant: 8).isActive = true
        label.trailingAnchor.constraint(equalTo: self.contentView.trailingAnchor, constant: -8).isActive = true
        label.widthAnchor.constraint(greaterThanOrEqualToConstant: 10).isActive = true
        label.heightAnchor.constraint(equalToConstant: 20).isActive = true
        
        return label
    }()
    
    lazy var iconView: UIImageView = {
        let imageView = UIImageView()
        imageView.contentMode = .scaleAspectFill
        imageView.clipsToBounds = true
        imageView.backgroundColor = .lightGray
        imageView.translatesAutoresizingMaskIntoConstraints = false
        self.contentView.addSubview(imageView)
        imageView.leadingAnchor.constraint(equalTo: self.contentView.leadingAnchor, constant: 16).isActive = true
        imageView.centerYAnchor.constraint(equalTo: self.contentView.centerYAnchor).isActive = true
        imageView.widthAnchor.constraint(equalToConstant: 50).isActive = true
        imageView.heightAnchor.constraint(equalToConstant: 50).isActive = true
        return imageView
    }()
    
    lazy var detailsLabel: UILabel = {
        let label = UILabel()
        label.font = .italicSystemFont(ofSize: 11)
        label.textColor = .lightGray
        label.numberOfLines = 1
        label.translatesAutoresizingMaskIntoConstraints = false
        self.contentView.addSubview(label)
        label.topAnchor.constraint(equalTo: self.titleLabel.bottomAnchor, constant: 8).isActive = true
        label.leadingAnchor.constraint(equalTo: self.titleLabel.leadingAnchor).isActive = true
        label.trailingAnchor.constraint(equalTo: self.contentView.trailingAnchor, constant: -8).isActive = true

        label.heightAnchor.constraint(equalToConstant: 16).isActive = true
        return label
    }()
    
    lazy var priceLabel: UILabel = {
        let label = UILabel()
        label.font = .systemFont(ofSize: 12)
        label.numberOfLines = 1
        label.translatesAutoresizingMaskIntoConstraints = false
        self.contentView.addSubview(label)
  
        label.bottomAnchor.constraint(equalTo: self.contentView.bottomAnchor, constant: -8).isActive = true
        label.trailingAnchor.constraint(equalTo: self.contentView.trailingAnchor, constant: -8).isActive = true
        
        label.widthAnchor.constraint(greaterThanOrEqualToConstant: 40).isActive = true
        
        return label
    }()
    
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    func update(_ item: ItemCellPresentable) {
        titleLabel.text = (item.cellUrgence ? "‼️ " : "") + item.cellTitle
        detailsLabel.text = item.cellCategory
        priceLabel.text = item.cellPrice
        iconView.image = nil
        
        self.iconView.retrieveImage(fromURL: item.cellImageURL, completion: { [weak self] (image, url) in
            DispatchQueue.main.async {
                guard let image = image, let url = url, url == item.cellImageURL else {
                    self?.iconView.image = nil
                    return
                }
                self?.iconView.image = image
            }
        })
    }
    
}
