//
//  Router.swift
//  LeBonCoin
//
//  Created by Armel Fardel on 14/02/2020.
//  Copyright © 2020 Armel Fardel. All rights reserved.
//

import Foundation
import UIKit

protocol Routing {
    func showNavigationController()
    func showNextViewController()
    func showBackButton()
}



