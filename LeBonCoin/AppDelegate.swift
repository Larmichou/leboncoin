//
//  AppDelegate.swift
//  LeBonCoin
//
//  Created by Armel Fardel on 09/02/2020.
//  Copyright © 2020 Armel Fardel. All rights reserved.
//

import UIKit

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {

    var window: UIWindow?

    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        
        self.window = UIWindow(frame: UIScreen.main.bounds)
        
        let vc = LoadingViewController()
        vc.launchRequest()
        self.window?.rootViewController = NavigationController(rootViewController: vc)
        self.window?.makeKeyAndVisible()

        return true
    }
}

