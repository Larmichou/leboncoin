//
//  Category.swift
//  LeBonCoin
//
//  Created by Armel Fardel on 09/02/2020.
//  Copyright © 2020 Armel Fardel. All rights reserved.
//

import Foundation

struct Category: Codable {
    let id: Int
    let name: String
}
