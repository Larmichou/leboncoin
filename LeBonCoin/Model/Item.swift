//
//  Item.swift
//  LeBonCoin
//
//  Created by Armel Fardel on 09/02/2020.
//  Copyright © 2020 Armel Fardel. All rights reserved.
//

import Foundation

struct Item: Codable {
    let id: Int
    let categoryId: Int
    let title: String
    let desc: String
    let price: Double
    let creationDate: Date
    let urgent: Bool
    let imagesURLs: ImageURLs
    
    enum CodingKeys: String, CodingKey {
        case id
        case categoryId = "category_id"
        case title
        case desc = "description"
        case price
        case creationDate = "creation_date"
        case urgent = "is_urgent"
        case imagesURLs = "images_url"
    }
}
