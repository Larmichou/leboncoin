//
//  ImageURLs.swift
//  LeBonCoin
//
//  Created by Armel Fardel on 14/02/2020.
//  Copyright © 2020 Armel Fardel. All rights reserved.
//

import Foundation

struct ImageURLs: Codable {
    let small: URL?
    let thumbnail: URL?
    
    enum CodingKeys: String, CodingKey {
        case small
        case thumbnail = "thumb"
    }
    
    init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)
        if let imageURLSmallString = try container.decodeIfPresent(String.self, forKey: .small) {
            self.small = URL(string: imageURLSmallString)
        } else {
            self.small = nil
        }
        if let imageURLThumbnailString = try container.decodeIfPresent(String.self, forKey: .thumbnail) {
            self.thumbnail = URL(string: imageURLThumbnailString)
        } else {
            self.thumbnail = nil
        }
    }
}
