//
//  ImageRequester.swift
//  LeBonCoin
//
//  Created by Armel Fardel on 14/02/2020.
//  Copyright © 2020 Armel Fardel. All rights reserved.
//

import Foundation
import UIKit

struct ImageRequester: ImageFetcher {
    
    var cacher: ImageCacher

    init(cacher: ImageCacher = UserDefaultCacher()) {
        self.cacher = cacher
    }
    
    func retrieveImage(fromURL url: URL?, completion: ((UIImage?) -> Void)?) {
        guard let url = url else {
            completion?(nil)
            return
        }
        
        if let image = cacher.retrieveImage(atURL: url) {
            completion?(image)
        } else {
            URLSession.shared.dataTask(with: url) { (data, response, error) in
                guard let data = data,
                    error == nil else {
                        completion?(nil)
                        return
                }
                self.cacher.cache(data, forURL: url)
                completion?(UIImage(data: data))
            }.resume()
        }
    }
}
