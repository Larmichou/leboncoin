//
//  UserDefaultCacher.swift
//  LeBonCoin
//
//  Created by Armel Fardel on 14/02/2020.
//  Copyright © 2020 Armel Fardel. All rights reserved.
//

import Foundation
import UIKit

struct UserDefaultCacher: ImageCacher {
    
    func cache(_ imageData: Data, forURL url: URL) {
        UserDefaults.standard.set(imageData, forKey: url.absoluteString)
    }
    
    func retrieveImage(atURL url: URL) -> UIImage? {
        guard let data = UserDefaults.standard.data(forKey: url.absoluteString) else { return nil }
        return UIImage(data: data)
    }
}
