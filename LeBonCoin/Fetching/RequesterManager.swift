//
//  Requester.swift
//  LeBonCoin
//
//  Created by Armel Fardel on 10/02/2020.
//  Copyright © 2020 Armel Fardel. All rights reserved.
//

import Foundation
import UIKit

class RequesterManager {

    static var shared: RequesterManager = RequesterManager()
    
    private let dataFetcher: DataFetcher
    private let imageFetcher: ImageFetcher
    
    init(dataFetcher: DataFetcher = HTTPRequester(), imageFetcher: ImageFetcher = ImageRequester()) {
        self.dataFetcher = dataFetcher
        self.imageFetcher = imageFetcher
    }
    
    var categories: [Category] = []
}

extension RequesterManager: DataFetcher {
    
    func retrieveCategories(completion: ((Result<[Category], Error>) -> Void)?) {
        dataFetcher.retrieveCategories { [weak self] (result) in
            switch result {
            case .success(let categories):
                self?.categories = categories
            default:
                break
            }
            completion?(result)
        }
    }
    
    func retrieveItems(completion: ((Result<[Item], Error>) -> Void)?) {
        dataFetcher.retrieveItems(completion: completion)
    }
}

extension RequesterManager: ImageFetcher {
    func retrieveImage(fromURL url: URL?, completion: ((UIImage?) -> Void)?) {
        imageFetcher.retrieveImage(fromURL: url, completion: completion)
    }
}
