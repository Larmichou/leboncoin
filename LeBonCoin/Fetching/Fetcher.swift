//
//  Fetcher.swift
//  LeBonCoin
//
//  Created by Armel Fardel on 14/02/2020.
//  Copyright © 2020 Armel Fardel. All rights reserved.
//

import Foundation
import UIKit

protocol DataFetcher {
    func retrieveCategories(completion: ((Result<[Category], Error>) -> Void)?)
    func retrieveItems(completion: ((Result<[Item], Error>) -> Void)?)
}

protocol ImageFetcher {
    func retrieveImage(fromURL url: URL?, completion: ((UIImage?) -> Void)?)
}

protocol ImageCacher {
    func cache(_ imageData: Data, forURL url: URL)
    func retrieveImage(atURL url: URL) -> UIImage?
}
