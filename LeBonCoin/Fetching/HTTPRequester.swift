//
//  HTTPRequester.swift
//  LeBonCoin
//
//  Created by Armel Fardel on 10/02/2020.
//  Copyright © 2020 Armel Fardel. All rights reserved.
//

import Foundation
import UIKit


struct HTTPRequester: DataFetcher {
    
    enum HTTPRequesterErrors: Error {
        case badURL
    }
    
    func retrieveCategories(completion: ((Result<[Category], Error>) -> Void)?) {
        guard let url = URL(string: "https://raw.githubusercontent.com/leboncoin/paperclip/master/categories.json") else {
            completion?(.failure(HTTPRequesterErrors.badURL))
            return
        }
        
        URLSession.shared.dataTask(with: url) { (data, response, error) in
            
            if let error = error {
                completion?(.failure(error))
                return
            }
            guard let data = data else {
                completion?(.success([]))
                return
            }
            do {
                let categories = try JSONDecoder().decode([Category].self, from: data)
                completion?(.success(categories))
            } catch let decoderError {
                print("Error: \(decoderError)")
                completion?(.failure(decoderError))
            }
        }.resume()
    }
    
    func retrieveItems(completion: ((Result<[Item], Error>) -> Void)?) {
        guard let url = URL(string: "https://raw.githubusercontent.com/leboncoin/paperclip/master/listing.json") else {
            completion?(.failure(HTTPRequesterErrors.badURL))
            return
        }
        
        URLSession.shared.dataTask(with: url) { (data, response, error) in
            if let error = error {
                completion?(.failure(error))
                return
            }

            guard let data = data else {
                completion?(.success([]))
                return
            }
            do {
                let decoder = JSONDecoder()
                decoder.dateDecodingStrategy = .iso8601
                let items = try decoder.decode([Item].self, from: data)
                completion?(.success(items))
            } catch let decoderError {
                print("Error: \(decoderError)")
                completion?(.failure(decoderError))
            }
        }.resume()
    }

}
