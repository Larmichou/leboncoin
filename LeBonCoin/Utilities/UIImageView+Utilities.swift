//
//  UIImageView+Utilities.swift
//  LeBonCoin
//
//  Created by Armel Fardel on 14/02/2020.
//  Copyright © 2020 Armel Fardel. All rights reserved.
//

import UIKit

extension UIImageView {
    
    func retrieveImage(fromURL url: URL?, completion: ((UIImage?, URL?) -> Void)?) {
        guard let url = url else {
            completion?(nil, nil)
            return
        }
        
        RequesterManager.shared.retrieveImage(fromURL: url) { (image) in
            completion?(image, url)
        }
    }
}
