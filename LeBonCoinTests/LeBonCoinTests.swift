//
//  LeBonCoinTests.swift
//  LeBonCoinTests
//
//  Created by Armel Fardel on 09/02/2020.
//  Copyright © 2020 Armel Fardel. All rights reserved.
//

import XCTest
@testable import LeBonCoin

class LeBonCoinTests: XCTestCase {

    override func setUp() {
        // Put setup code here. This method is called before the invocation of each test method in the class.
    }

    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
    }

    func testValidItemJSON() {
        
        let jsonString = """
{
   "id":1702201501,
   "category_id":5,
   "title":"Sac de couchage duvet Carinthia",
   "description":"Sac de couchage de la marque Carinthia en duvet.  Avec collerette rabat de chaleur.",
   "price":60.00,
   "images_url":{
      "small":"https://raw.githubusercontent.com/leboncoin/paperclip/master/ad-small/e06a51b2bf23bef81747069663d4fbfd03b6c889.jpg",
      "thumb":"https://raw.githubusercontent.com/leboncoin/paperclip/master/ad-thumb/e06a51b2bf23bef81747069663d4fbfd03b6c889.jpg"
   },
   "creation_date":"2019-11-06T11:21:22+0000",
   "is_urgent":false
}
"""
        let jsonData = jsonString.data(using: .utf8)!
        let decoder = JSONDecoder()
        decoder.dateDecodingStrategy = .iso8601
        do {
            let item = try decoder.decode(Item.self, from: jsonData)
            XCTAssertNotNil(item)
        } catch {
            XCTFail("\(error)")
        }
    }
    
    func testInvalidItemJSON() {
        let jsonString = """
        {
           "id":1702201501,
           "title":"Sac de couchage duvet Carinthia",
           "description":"Sac de couchage de la marque Carinthia en duvet.  Avec collerette rabat de chaleur.",
           "price":60.00,
           "images_url":{
              "small":"https://raw.githubusercontent.com/leboncoin/paperclip/master/ad-small/e06a51b2bf23bef81747069663d4fbfd03b6c889.jpg",
              "thumb":"https://raw.githubusercontent.com/leboncoin/paperclip/master/ad-thumb/e06a51b2bf23bef81747069663d4fbfd03b6c889.jpg"
           },
           "creation_date":"2019-11-06T11:21:22+0000",
           "is_urgent":false
        }
        """
        let jsonData = jsonString.data(using: .utf8)!
        let decoder = JSONDecoder()
        decoder.dateDecodingStrategy = .iso8601
        do {
             _ = try decoder.decode(Item.self, from: jsonData)
            XCTFail("JSON is invalid there shouldn't be a success")
        } catch {
            XCTAssertNotNil(error)
        }
    }



    func testPerformanceExample() {
        // This is an example of a performance test case.
        self.measure {
            // Put the code you want to measure the time of here.
        }
    }

}
