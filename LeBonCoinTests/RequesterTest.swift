//
//  RequesterTest.swift
//  LeBonCoinTests
//
//  Created by Armel Fardel on 10/02/2020.
//  Copyright © 2020 Armel Fardel. All rights reserved.
//

import UIKit
import XCTest
@testable import LeBonCoin

struct EmptyDataFetcher: DataFetcher {
    func retrieveCategories(completion: ((Result<[Category], Error>) -> Void)?) {
        completion?(.success([]))
    }
    
    func retrieveItems(completion: ((Result<[Item], Error>) -> Void)?) {
        completion?(.success([]))
    }
}

class RequesterTest: XCTestCase {
    
    func testRequesterManagerWithEmptyDataFetcher() {
        RequesterManager(dataFetcher: EmptyDataFetcher()).retrieveItems { (result) in
            switch result {
            case .success(let items):
                XCTAssert(items.isEmpty == true)
            case .failure:
                XCTFail("Even if empty not error shouldn't be thrown")
            }
        }
    }
    
    func testEmptyCategories() {
        EmptyDataFetcher().retrieveCategories { (result) in
            switch result {
            case .success(let categories):
                XCTAssert(categories.isEmpty == true)
            case .failure:
                XCTFail("Even if empty not error shouldn't be thrown")
            }
        }
    }
}
