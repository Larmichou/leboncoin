
I coded the model part quite early, but started the rest only the last couple days.

I added only 2 unit tests, just to show that I know where they are and how to do them:
One on validating the JSON, and another one to show that the Fetching files can be tested separately and part by part (the test doesn't make really sense by itself).

Requester is a singleton (`shared`), to make it easily accessible from anywhere.
It has default initialization values which are the one used in the app, but can be adapted in tests by instance.
The default `ImageCacher` is using `UserDefaults` (`UserDefaultCacher`), because it's the fastest way to have one.
It's not recommended to use `UserDefaults` for that, but it's just to show that I thought about caching, and that it can be tested and replaced.
For a demo app, I think that's sufficient modularity.

Since the app is quite simple, I didn't create presenters (that would have been too much code, I prefer efficiency, especially for a sample demo code), but added protocols (ItemListPresenter, ItemDetailsPresenter) to do so that could be used by one. I made the ViewControllers directly be compliant with them.

For the `UIImageView` utility, I'd recommend to use a solution like SDWebImage, Alamofire+Image, etc. There are more optimizations and customizations (queue, placeholders, loader, cancel, caching, etc.)
